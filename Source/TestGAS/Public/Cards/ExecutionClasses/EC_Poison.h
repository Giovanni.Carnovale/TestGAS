// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameplayEffectExecutionCalculation.h"
#include "EC_Poison.generated.h"

/**
 * 
 */
UCLASS()
class TESTGAS_API UEC_Poison : public UGameplayEffectExecutionCalculation
{
	GENERATED_BODY()

	
public:
	void Execute_Implementation(const FGameplayEffectCustomExecutionParameters& ExecutionParams, FGameplayEffectCustomExecutionOutput& OutExecutionOutput) const override;

};
