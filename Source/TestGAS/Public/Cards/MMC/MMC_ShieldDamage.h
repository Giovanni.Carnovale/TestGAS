// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameplayModMagnitudeCalculation.h"
#include "MMC_ShieldDamage.generated.h"

/**
 * 
 */
UCLASS()
class TESTGAS_API UMMC_ShieldDamage : public UGameplayModMagnitudeCalculation
{
	GENERATED_BODY()

	FGameplayEffectAttributeCaptureDefinition TargetShield;
	FGameplayEffectAttributeCaptureDefinition SourceBuffAttack;

	UMMC_ShieldDamage();

	virtual float CalculateBaseMagnitude_Implementation(const FGameplayEffectSpec& Spec) const override;
};
