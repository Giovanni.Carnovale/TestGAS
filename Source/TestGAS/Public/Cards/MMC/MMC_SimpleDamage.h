// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameplayModMagnitudeCalculation.h"
#include "MMC_SimpleDamage.generated.h"

/**
 * 
 */
UCLASS(Blueprintable)
class TESTGAS_API UMMC_SimpleDamage : public UGameplayModMagnitudeCalculation
{
	GENERATED_BODY()
	
	FGameplayEffectAttributeCaptureDefinition TargetShield;
	FGameplayEffectAttributeCaptureDefinition SourceBuffAttack;

	UMMC_SimpleDamage();
	
	virtual float CalculateBaseMagnitude_Implementation(const FGameplayEffectSpec& Spec) const override;
};
