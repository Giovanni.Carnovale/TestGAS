// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Abilities/GameplayAbility.h"
#include "CardAbility.generated.h"

/**
 * 
 */
UCLASS(Blueprintable)
class TESTGAS_API UCardAbility : public UGameplayAbility
{
	GENERATED_BODY()
	
public:

	UCardAbility();

	float GetManaCost() const;

protected:

UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "Card Paramters")
float manaCost;

};
