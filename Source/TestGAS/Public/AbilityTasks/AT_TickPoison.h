// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Abilities/Tasks/AbilityTask.h"
#include "TestGAS/TestGASCharacter.h"
#include "AT_TickPoison.generated.h"

/**
 * 
 */
class UAbilitySystemComponent;

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FDurationEnd);


UCLASS(Blueprintable)
class TESTGAS_API UAT_TickPoison : public UAbilityTask
{
	GENERATED_BODY()
	
	UFUNCTION(BlueprintCallable, Category="Ability|Tasks")
	static UAT_TickPoison* TickPoison(
		UGameplayAbility* OwningAbility, FName TasksInstanceName, float duration, 
		float damage, TSubclassOf<UGameplayEffect> Effect, UAbilitySystemComponent* Target);

	void OnDestroy(bool bInOwnerFinished) override;
	UFUNCTION()
	void TickEffect();

	UPROPERTY(BlueprintAssignable, BlueprintCallable, Category = "Delegates")
	FDurationEnd OnDurationEnd;

protected:
	float CurrentDuration;
	float DamagePerTick;

	TSubclassOf<UGameplayEffect> _Effect;
	UAbilitySystemComponent* _Target;

	void Activate() override;
};
