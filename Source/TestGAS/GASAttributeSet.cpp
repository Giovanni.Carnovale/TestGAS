// Fill out your copyright notice in the Description page of Project Settings.


#include "GASAttributeSet.h"
#include "Net/UnrealNetwork.h"
#include "GameplayEffectExtension.h"

UGASAttributeSet::UGASAttributeSet()
{

}

void UGASAttributeSet::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME_CONDITION_NOTIFY(UGASAttributeSet, Health, COND_None, REPNOTIFY_Always);
	DOREPLIFETIME_CONDITION_NOTIFY(UGASAttributeSet, Mana, COND_None, REPNOTIFY_Always);
}

void UGASAttributeSet::OnRep_Health(const FGameplayAttributeData& oldHealth)
{
	GAMEPLAYATTRIBUTE_REPNOTIFY(UGASAttributeSet, Health, oldHealth);
}

void UGASAttributeSet::OnRep_Mana(const FGameplayAttributeData& oldMana)
{
	GAMEPLAYATTRIBUTE_REPNOTIFY(UGASAttributeSet, Mana, oldMana);
}


void UGASAttributeSet::PostGameplayEffectExecute(const FGameplayEffectModCallbackData& Data)
{
	Super::PostGameplayEffectExecute(Data);

	float AttributeValue = Data.EvaluatedData.Attribute.GetNumericValue(this);

	if (Data.EvaluatedData.Attribute == GetHealthAttribute()) {
		SetHealth(FMath::Clamp(AttributeValue, 0.0f, 100.f));
	}

	if (Data.EvaluatedData.Attribute == GetManaAttribute()) {
		SetMana(FMath::Clamp(AttributeValue, 0.0f, 6.0f));
	}

	if (Data.EvaluatedData.Attribute == GetShieldAttribute()) {
		SetShield(FMath::Max(AttributeValue, 0.0f));
	}


	if (Data.EvaluatedData.Attribute == GetBuffAttackAttribute()) {
		SetBuffAttack(FMath::Max(0.0f, AttributeValue));
	}

	if (Data.EvaluatedData.Attribute == GetBuffShieldAttribute()) {
		SetBuffAttack(FMath::Max(0.0f, AttributeValue));
	}

	if (Data.EvaluatedData.Attribute == GetBuffAttackDurationAttribute()) {
		SetBuffAttack(FMath::Max(0.0f, AttributeValue));
	}

	if (Data.EvaluatedData.Attribute == GetBuffShieldDurationAttribute()) {
		SetBuffAttack(FMath::Max(0.0f, AttributeValue));
	}
}
