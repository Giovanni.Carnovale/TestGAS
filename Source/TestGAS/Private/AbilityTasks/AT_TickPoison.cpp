// Fill out your copyright notice in the Description page of Project Settings.


#include "AbilityTasks/AT_TickPoison.h"
#include "Kismet/GameplayStatics.h"
#include "GameplayAbilitiesClasses.h"
#include "AbilitySystemComponent.h"
#include "TestGAS/TestGASCharacter.h"



UAT_TickPoison* UAT_TickPoison::TickPoison(UGameplayAbility* OwningAbility, FName TasksInstanceName, float duration, float damage, TSubclassOf<UGameplayEffect> Effect, UAbilitySystemComponent* Target)
{
	UAT_TickPoison* Tick = NewAbilityTask<UAT_TickPoison>(OwningAbility, TasksInstanceName);
	Tick->CurrentDuration = duration;
	Tick->DamagePerTick = damage;

	APlayerController* pPController = UGameplayStatics::GetPlayerController(Tick->GetWorld(), 0);
	ATestGASCharacter* Player = pPController->GetPawn<ATestGASCharacter>();

	Player->OnTurnTick.AddDynamic(Tick, &UAT_TickPoison::TickEffect);

	Tick->_Effect = Effect;
	Tick->_Target = Target;

	return Tick;
}

void UAT_TickPoison::OnDestroy(bool bInOwnerFinished)
{
	Super::OnDestroy(bInOwnerFinished);

}

void UAT_TickPoison::Activate()
{

}

void UAT_TickPoison::TickEffect()
{
	if (CurrentDuration > 0) 
	{
		if (GEngine) GEngine->AddOnScreenDebugMessage(1, 1.0f, FColor::Green, L"Ticked poison");

		FGameplayEffectSpecHandle Handle = Ability->MakeOutgoingGameplayEffectSpec(_Effect, 0);
		Handle.Data.Get()->SetSetByCallerMagnitude(FGameplayTag::RequestGameplayTag(FName(TEXT("Effect.Value"))), DamagePerTick);

		AbilitySystemComponent->ApplyGameplayEffectSpecToTarget(*Handle.Data.Get(), _Target);
		CurrentDuration--;
	}
	else {
		OnDurationEnd.Broadcast();
		EndTask();
	}
}