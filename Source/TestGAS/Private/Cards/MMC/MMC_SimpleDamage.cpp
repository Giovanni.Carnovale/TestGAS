// Fill out your copyright notice in the Description page of Project Settings.


#include "Cards/MMC/MMC_SimpleDamage.h"
#include "TestGAS/GASAttributeSet.h"

UMMC_SimpleDamage::UMMC_SimpleDamage()
{
    TargetShield.AttributeToCapture = UGASAttributeSet::GetShieldAttribute();
    TargetShield.AttributeSource = EGameplayEffectAttributeCaptureSource::Target;
    SourceBuffAttack.AttributeToCapture = UGASAttributeSet::GetBuffAttackAttribute();
    SourceBuffAttack.AttributeSource = EGameplayEffectAttributeCaptureSource::Source;


    RelevantAttributesToCapture.Add(TargetShield);
    RelevantAttributesToCapture.Add(SourceBuffAttack);
}

float UMMC_SimpleDamage::CalculateBaseMagnitude_Implementation(const FGameplayEffectSpec& Spec) const
{
    float Damage = Spec.GetSetByCallerMagnitude(FGameplayTag::RequestGameplayTag(TEXT("Effect.Value")));
        
	FAggregatorEvaluateParameters EvaluationParameters;
    float BuffAttack = 0.f;
    GetCapturedAttributeMagnitude(SourceBuffAttack, Spec, EvaluationParameters, BuffAttack);

    Damage+=BuffAttack;

    float TargetShieldValue = 0.0f;
    GetCapturedAttributeMagnitude(TargetShield, Spec, EvaluationParameters, TargetShieldValue);

    Damage -= TargetShieldValue;
    Damage = FMath::Max(Damage, 0.0f);

    return -Damage;
}

