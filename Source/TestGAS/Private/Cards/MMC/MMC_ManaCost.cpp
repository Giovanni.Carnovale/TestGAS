// Fill out your copyright notice in the Description page of Project Settings.

#include "Cards/MMC/MMC_ManaCost.h"
#include "Cards/CardAbility.h"


float UMMC_ManaCost::CalculateBaseMagnitude_Implementation(const FGameplayEffectSpec& Spec) const
{

	const UCardAbility* Card = Cast<UCardAbility>(Spec.GetContext().GetAbilityInstance_NotReplicated());

	if (!Card)
	{
		return 0.0f;
	}

	return Card->GetManaCost();
}
