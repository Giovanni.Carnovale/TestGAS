// Fill out your copyright notice in the Description page of Project Settings.


#include "Cards/ExecutionClasses/EC_Poison.h"

void UEC_Poison::Execute_Implementation(const FGameplayEffectCustomExecutionParameters& ExecutionParams, FGameplayEffectCustomExecutionOutput& OutExecutionOutput) const
{
	Super::Execute_Implementation(ExecutionParams, OutExecutionOutput);
	

	const FGameplayEffectSpec& Spec = ExecutionParams.GetOwningSpec();
	Spec.GetSetByCallerMagnitude(FGameplayTag::RequestGameplayTag(FName("Effect.Duration")));
}
