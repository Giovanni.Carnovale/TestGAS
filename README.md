TEST GAS
========
Questa repository è un prototipo per un sistema di combattimento Turn Based per un gioco di Carte basato su Gameplay Ability System.

Il prototipo si basa sulla documentazione [GASDocumentation di Tranek](https://github.com/tranek/GASDocumentation) e il suo [Prototipo: GASShooter](https://github.com/tranek/GASShooter).

Componenti Implementate
---

Attualmente sono presenti:
1. Un semplice sistema di selezione delle carte da gioco.
2. Alcuni volumi per applicare effetti quando vi si entra a contatto
3. Tre carte d'esempio: una carta per infliggere danno semplice, una carta per infliggere uno status veleno, una pozione di cura
4. Un attributeSet che include Vita, mana, scudo, Buff per l'attacco, buff per gli scudi.
5. Diversi effetti: costi del mana, danneggiamento, veleno, healing, etc...
6. CustomCalculationClass per gestire il danno in maniera analoga a giochi come Slay The Spire
7. Alcune animazioni ed eventi basati su GameplayCues

Comandi
-------

Potete giocare le carte con i tasti 1, 2, 3
Potete avanzare di turno con Invio
Alcune carte richiedono un target, per selezionare un target basterà essere abbastanza vicini quando viene giocata la carta.
